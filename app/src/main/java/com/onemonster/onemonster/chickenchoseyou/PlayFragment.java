package com.onemonster.onemonster.chickenchoseyou;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class PlayFragment extends Fragment implements OnClickListener {

    public enum Option {
        ROCK, PAPER, SCISSORS
    }

    public enum Result {
        WIN, LOSS, DRAW
    }

    private Option userSelection;
    private Result gameResult;
    private int stageNumber; // 0 is stage_1
    final private int NUM_OF_GAMES_TO_WIN = 8;
    private int numberOfPlays;
    final private int NUM_OF_PLAYS_TO_AD = 20;
    private TypedArray stages;

    private ImageView chickenChoiceImage;
    private ImageView stageTextImage;
    private ImageView playResultImage;

    private ImageButton buttonRock;
    private ImageButton buttonPaper;
    private ImageButton buttonScissors;
    private ImageView userPick;

    private MainActivity mActivity;

    public PlayFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        numberOfPlays = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_play, container, false);
        buttonRock = (ImageButton) view.findViewById(R.id.buttonRock);
        buttonPaper = (ImageButton) view.findViewById(R.id.buttonPaper);
        buttonScissors = (ImageButton) view.findViewById(R.id.buttonScissors);

        buttonRock.setOnClickListener(this);
        buttonPaper.setOnClickListener(this);
        buttonScissors.setOnClickListener(this);

        stages  = getResources().obtainTypedArray(R.array.stages);
        stageNumber = 0;
        stageTextImage = (ImageView) view.findViewById(R.id.stageTextImage);
        stageTextImage.setImageResource(stages.getResourceId(stageNumber, -1));

        chickenChoiceImage = (ImageView) view.findViewById(R.id.chickenChoice);
        userPick = (ImageView) view.findViewById(R.id.userPick);

        playResultImage = (ImageView) view.findViewById(R.id.playResultImage);

        ImageButton information = (ImageButton) view.findViewById(R.id.information);
        information.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showInformation();
            }
        });
        showInformation();

        return view;
    }

    private void showInformation() {
        Typeface medium = Typeface.createFromAsset(mActivity.getAssets(), "fonts/NotoSansCJKkr-Medium.otf");
        final Dialog dialog = new Dialog(mActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_layout2);
        TextView dialogMessage = (TextView) dialog.findViewById(R.id.dialogMessage);
        ImageView frame = (ImageView) dialog.findViewById(R.id.dialog2FrameImage);
        frame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialogMessage.setTypeface(medium);
        dialogMessage.setText(getString(R.string.game_rules_explaination));
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity)context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonRock:
                userSelection = Option.ROCK;
                //buttonRock.setBackgroundResource(R.drawable.rock2);
                break;
            case R.id.buttonPaper:
                userSelection = Option.PAPER;
                //buttonPaper.setBackgroundResource(R.drawable.paper2);
                break;
            case R.id.buttonScissors:
                userSelection = Option.SCISSORS;
                //buttonScissors.setBackgroundResource(R.drawable.scissors2);
                break;
        }

        play();
        result();
    }

    private void play() {
        // Generates a random play.
        int rand =  ((int)(Math.random() * 10)) % 3;
        Option androidSelection = null;
        numberOfPlays += 1;
        if (numberOfPlays % NUM_OF_PLAYS_TO_AD == 0) {
            numberOfPlays = 0;
            mActivity.showFull();
        }

        // Sets the right image according to random selection.
        switch (rand) {
            case 0:
                androidSelection = Option.ROCK;
                chickenChoiceImage.setImageResource(R.drawable.chicken_rock);
                break;
            case 1:
                androidSelection = Option.PAPER;
                chickenChoiceImage.setImageResource(R.drawable.chicken_paper);
                break;
            case 2:
                androidSelection = Option.SCISSORS;
                chickenChoiceImage.setImageResource(R.drawable.chicken_scissors);
                break;
        }
        // Determine game result according to user selection and Android selection.
        if (androidSelection == userSelection) {
            gameResult = Result.DRAW;
        }
        else if (androidSelection == Option.ROCK && userSelection == Option.SCISSORS) {
            gameResult = Result.LOSS;
        }
        else if (androidSelection == Option.PAPER && userSelection == Option.ROCK) {
            gameResult = Result.LOSS;
        }
        else if (androidSelection == Option.SCISSORS && userSelection == Option.PAPER) {
            gameResult = Result.LOSS;
        } else {
            gameResult = Result.WIN;
        }
    }

    private void result() {

        Animation enlarge = AnimationUtils.loadAnimation(getContext(), R.anim.enlarge);
        Animation fadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        Animation stay = AnimationUtils.loadAnimation(getContext(), R.anim.stay);

        stay.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                disableButtons();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //buttonRock.setImageResource(R.drawable.rock);
                if (stageNumber >= NUM_OF_GAMES_TO_WIN) {
                    Animation fadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                    mActivity.callWinFragment();
                    buttonRock.startAnimation(fadeOut);
                    buttonPaper.startAnimation(fadeOut);
                    buttonScissors.startAnimation(fadeOut);
                } else {
                    chickenChoiceImage.setImageResource(R.drawable.chicken_question);
                    stageTextImage.setImageResource(stages.getResourceId(stageNumber, 0));
                    enableButtons();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // Sets the right chicken image according to result.
        if (gameResult == Result.LOSS) {
            stageNumber = 0;
            playResultImage.setImageResource(R.drawable.lose);
        } else if (gameResult == Result.WIN) {
            stageNumber += 1;
            playResultImage.setImageResource(R.drawable.win);
        } else {
            stageNumber = 0;
            playResultImage.setImageResource(R.drawable.draw);
        }

        // Sets center image to current pick.
        if (userSelection == Option.ROCK) {
            userPick.setImageResource(R.drawable.rock);
        } else if (userSelection == Option.PAPER) {
            userPick.setImageResource(R.drawable.paper);
        } else if (userSelection == Option.SCISSORS) {
            userPick.setImageResource(R.drawable.scissors);
        }
        buttonRock.startAnimation(fadeOut);
        buttonPaper.startAnimation(fadeOut);
        buttonScissors.startAnimation(fadeOut);
        playResultImage.startAnimation(enlarge);
        userPick.startAnimation(stay);
    }

    private void disableButtons() {
        buttonRock.setEnabled(false);
        buttonPaper.setEnabled(false);
        buttonScissors.setEnabled(false);
    }

    private void enableButtons() {
        buttonRock.setEnabled(true);
        buttonPaper.setEnabled(true);
        buttonScissors.setEnabled(true);
    }
}
